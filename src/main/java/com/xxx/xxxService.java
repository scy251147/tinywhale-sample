package com.xxx;

import com.tw.client.HelloService;
import com.tw.core.TinyWhaleProxy;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author shichaoyang
 * @Description:
 * @date 2019-03-28 12:53
 */
public class xxxService {

    @Autowired
    private TinyWhaleProxy rpcProxy;

    private HelloService helloService;

    public String invoke(String message) throws InterruptedException {
        if(helloService == null) {
            helloService = rpcProxy.create(HelloService.class);
        }
        String result = helloService.hello(message);
        return result;
    }

}
