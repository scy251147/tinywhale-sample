package com.tw;

import com.xxx.xxxService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.TimeUnit;

/**
 * @author shichaoyang
 * @Description:
 * @date 2019-03-26 18:39
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:spring-config.xml"})
public class ClientTest {

    @Autowired
    private xxxService aService;

    @Test
    public void testClient() throws InterruptedException {
        for (int i = 0; i < 1000; i++) {
            long start = System.currentTimeMillis();
            String result = aService.invoke("scy" + i);
            long end = System.currentTimeMillis();
            System.out.println("---->" + result);
            System.out.println("time cost[" + i + "]:" + (end - start));
        }
    }
}
