package com.tw;

import com.tw.client.HelloService;
import com.tw.core.TinyWhaleProxy;
import com.tw.core.serverManage.ServerDiscovery;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import java.util.concurrent.TimeUnit;

/**
 * @author shichaoyang
 * @Description:
 * @date 2019-03-29 18:39
 */
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Thread)
public class ProxyBenchmark {

    private ServerDiscovery serviceDiscovery;

    private HelloService helloService;

    private TinyWhaleProxy tinyWhaleProxy;

    @Setup
    public void init() {
        tinyWhaleProxy = new TinyWhaleProxy();

        serviceDiscovery = new ServerDiscovery("127.0.0.1:2181");
        tinyWhaleProxy.setServiceDiscovery(serviceDiscovery);

        helloService = tinyWhaleProxy.create(HelloService.class);
    }

    @Benchmark
    @GroupThreads(4)
    public String test() {
        String result = helloService.hello("sdfsdfsdf");
        return result;
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(ProxyBenchmark.class.getSimpleName())
                .forks(1)
                .build();
        new Runner(opt).run();
    }
}